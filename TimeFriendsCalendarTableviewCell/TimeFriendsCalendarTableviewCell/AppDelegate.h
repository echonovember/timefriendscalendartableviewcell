//
//  AppDelegate.h
//  TimeFriendsCalendarTableviewCell
//
//  Created by Сергей on 20.01.16.
//  Copyright © 2016 Сергей. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;


@end

