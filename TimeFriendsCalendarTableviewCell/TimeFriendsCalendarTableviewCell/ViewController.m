//
//  ViewController.m
//  TimeFriendsCalendarTableviewCell
//
//  Created by Сергей on 20.01.16.
//  Copyright © 2016 Сергей. All rights reserved.
//

#import "ViewController.h"

@interface ViewController () <UITableViewDelegate, UITableViewDataSource>

@property (weak, nonatomic) IBOutlet UITableView *tableView;

@end

@implementation ViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view, typically from a nib.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - UITableViewDelegate

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    if (indexPath.row == 0) {
        return 150;
    }
    return tableView.rowHeight;
}

#pragma mark - UITableViewDataSource

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return 1;
}

// Row display. Implementers should *always* try to reuse cells by setting each cell's reuseIdentifier and querying for available reusable cells with dequeueReusableCellWithIdentifier:
// Cell gets various attributes set automatically based on table (separators) and data source (accessory views, editing controls)

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    UIImageView *userImageView;
    UILabel *userNameLabel;
    UILabel *startTimeLabel, *hyphenLabel, *endTimeLabel;
    UILabel *recordDetailLabel;
    UIImageView *transportTypeImageView;
    UILabel *recordPlaceNameLabel;
    UIButton *mapButton, *inviteButton, *showCommentsButton;
    
    static NSString *RecordCellIdentifier = @"RecordCellIdentifier";
    
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:RecordCellIdentifier];
    if (cell == nil) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:RecordCellIdentifier];
        
        //        cell.backgroundColor = [UIColor groupTableViewBackgroundColor];
        
        CGSize userImageViewSize = CGSizeMake(75, 75);
        
        userImageView = [[UIImageView alloc] initWithFrame:CGRectMake(15, 30, userImageViewSize.width, userImageViewSize.height)];
        userImageView.backgroundColor = [UIColor grayColor];
        userImageView.layer.cornerRadius = userImageViewSize.height/2;
        userImageView.clipsToBounds = YES;
        [cell.contentView addSubview:userImageView];
        
        userNameLabel = [[UILabel alloc] initWithFrame:CGRectZero];
        userNameLabel.text = @"alexxander";
        [userNameLabel sizeToFit];
        userNameLabel.center = userImageView.center;
        CGRect userNameLabelFrame = userNameLabel.frame;
        userNameLabelFrame.origin.y += 50;
        userNameLabel.frame = userNameLabelFrame;
        [cell.contentView addSubview:userNameLabel];
        
        startTimeLabel = [[UILabel alloc] initWithFrame:CGRectMake(CGRectGetMaxX(userImageView.frame) + 15, 15, 60, 35)];
        [startTimeLabel.layer setCornerRadius:7];
        [startTimeLabel.layer setBorderWidth:1];
        [startTimeLabel.layer setBorderColor:[UIColor lightGrayColor].CGColor];
        startTimeLabel.layer.masksToBounds = YES;
        startTimeLabel.text = @"17:00";
        startTimeLabel.textAlignment = NSTextAlignmentCenter;
        [cell.contentView addSubview:startTimeLabel];
        
        hyphenLabel = [[UILabel alloc] initWithFrame:CGRectMake(CGRectGetMaxX(startTimeLabel.frame) + 4, 20, 0, 0)];
        hyphenLabel.text = @"-";
        [hyphenLabel sizeToFit];
        hyphenLabel.textAlignment = NSTextAlignmentCenter;
        [cell.contentView addSubview:hyphenLabel];
        
        endTimeLabel = [[UILabel alloc] initWithFrame:CGRectMake(CGRectGetMaxX(startTimeLabel.frame) + 15, 15, 60, 35)];
        [endTimeLabel.layer setCornerRadius:7];
        [endTimeLabel.layer setBorderWidth:1];
        [endTimeLabel.layer setBorderColor:[UIColor lightGrayColor].CGColor];
        endTimeLabel.layer.masksToBounds = YES;
        endTimeLabel.text = @"18:00";
        endTimeLabel.textAlignment = NSTextAlignmentCenter;
        [cell.contentView addSubview:endTimeLabel];
        
        recordDetailLabel = [[UILabel alloc] initWithFrame:CGRectMake(CGRectGetMaxX(userImageView.frame) + 15,
                                                                      CGRectGetMaxY(startTimeLabel.frame) + 10,
                                                                      tableView.frame.size.width/2,
                                                                      55)];
        recordDetailLabel.font = [UIFont systemFontOfSize:13];
        recordDetailLabel.text = @"Выбрать рюкзак Андрею на день рождения";
        recordDetailLabel.textAlignment = NSTextAlignmentLeft;
        recordDetailLabel.lineBreakMode = NSLineBreakByClipping;
        recordDetailLabel.numberOfLines = 0;
        [recordDetailLabel sizeToFit];
        [cell.contentView addSubview:recordDetailLabel];
        
        transportTypeImageView = [[UIImageView alloc] initWithFrame:CGRectMake(CGRectGetMaxX(userImageView.frame) + 15,
                                                                               CGRectGetMaxY(recordDetailLabel.frame) + 5,
                                                                               25,
                                                                               25)];
        transportTypeImageView.image = [UIImage imageNamed:@"transportTypeImage"];
        [cell.contentView addSubview:transportTypeImageView];
        
        recordPlaceNameLabel = [[UILabel alloc] initWithFrame:CGRectZero];
        recordPlaceNameLabel.font = [UIFont systemFontOfSize:13];
        recordPlaceNameLabel.text = @"ТЦ Мега Химки";
        [recordPlaceNameLabel sizeToFit];
        recordPlaceNameLabel.center = transportTypeImageView.center;
        CGRect recordPlaceNameLabelFrame = recordPlaceNameLabel.frame;
        recordPlaceNameLabelFrame.origin.x = CGRectGetMaxX(transportTypeImageView.frame) + 5;
        recordPlaceNameLabel.frame = recordPlaceNameLabelFrame;
        [cell.contentView addSubview:recordPlaceNameLabel];
        
        CGSize buttonSize = CGSizeMake(40, 40);
        
        inviteButton = [[UIButton alloc] initWithFrame:CGRectMake(CGRectGetMaxX(tableView.frame) - 50,
                                                                  75 - buttonSize.height/2,
                                                                  buttonSize.width,
                                                                  buttonSize.height)];
        inviteButton.backgroundColor = [UIColor blueColor];
        [inviteButton addTarget:self action:@selector(inviteButtonaction:) forControlEvents:UIControlEventTouchUpInside];
        [cell.contentView addSubview:inviteButton];
        
        mapButton = [[UIButton alloc] initWithFrame:CGRectMake(CGRectGetMinX(inviteButton.frame),
                                                               CGRectGetMidY(inviteButton.frame) - buttonSize.height - 25,
                                                               buttonSize.width,
                                                               buttonSize.height)];
        mapButton.backgroundColor = [UIColor blackColor];
        [mapButton addTarget:self action:@selector(mapButtonAction:) forControlEvents:UIControlEventTouchUpInside];
        [cell.contentView addSubview:mapButton];
        
        showCommentsButton = [[UIButton alloc] initWithFrame:CGRectMake(CGRectGetMinX(inviteButton.frame),
                                                                        CGRectGetMaxY(inviteButton.frame) + 5,
                                                                        buttonSize.width,
                                                                        buttonSize.height)];
        showCommentsButton.backgroundColor = [UIColor redColor];
        [showCommentsButton addTarget:self action:@selector(showCommentsButtonAction:) forControlEvents:UIControlEventTouchUpInside];
        [cell.contentView addSubview:showCommentsButton];
    }
    return cell;
}

@end
